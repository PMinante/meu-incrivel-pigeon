package com.example.pigeon_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

const val USER_ID = 0
const val OTHER_ID = 1

class MainActivity : AppCompatActivity() {
    private var fromUser:Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpListeners()
        setUpRecyclerView()
    }

    private fun setUpListeners() {
        sendButton.setOnClickListener{
            val messageText = messageEditText.text.toString()
            messageEditText.setText("")

            val adapter = messageList.adapter

            if(adapter is MessageAdapter){

                val message = ChatMessage(
                    messageText,
                    if(fromUser) USER_ID else OTHER_ID
                )

                adapter.addItems(message)

                messageList.scrollToPosition(adapter.itemCount - 1)

                fromUser = !fromUser
            }
        }
    }

    private fun setUpRecyclerView() {
        messageList.layoutManager = LinearLayoutManager(this)
        messageList.adapter = MessageAdapter()
    }
}