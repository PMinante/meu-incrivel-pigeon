package com.example.pigeon_app

import java.text.SimpleDateFormat
import java.util.*

class ChatMessage(val text: String,
                  val senderId: Int,
                  private val timestamp: Long = Date().time){
    val moment: String
    get() = SimpleDateFormat("HH:mm").format(timestamp)
}